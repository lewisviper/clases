﻿using System;
using BaseCalculadora;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("digite el primer valor");
            double valor1 = System.Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("digite el segundo valor");
            double valor2 = System.Convert.ToDouble(Console.ReadLine());

            Console.WriteLine("digite el Operador");
            string operador = Console.ReadLine();

            double resultado = 0;

            OperadoresBasicos operadores = new OperadoresBasicos();

            switch (operador)
            {
                case "/":
                    resultado = operadores.division(valor1, valor2);
                    break;
                case "*":
                    resultado = operadores.multiplicacion(valor1, valor2);
                    break;
                case "-":
                    resultado = operadores.resta(valor1, valor2);
                    break;
                case "+":
                    resultado = operadores.sumar(valor1, valor2);
                    break;
                default:
                    resultado = valor1;
                    break;
            }

            Console.WriteLine("Resultado");
            Console.WriteLine(resultado.ToString());
        }        
    }
}
