﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Windows.Forms;

namespace BasedeDatos
{
    public class ManejoBD
    {
        string strCadenaConexion ;

        public ManejoBD()
        {
            this.strCadenaConexion = "Data Source=localhost;Initial Catalog=pruebas;User ID=pruebas;Password=123456789;";
        }
                
        public List<Estructuras.TablaPadre1> LoadData(string stringSQL)
        {
            List<Estructuras.TablaPadre1> response = new List<Estructuras.TablaPadre1>();

            SqlConnection connection = new SqlConnection(strCadenaConexion);
            try
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlCommand sqlCommand = new SqlCommand(stringSQL, connection);
                    SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);
                    DataTable tabla = new DataTable();
                    adapter.Fill(tabla);
                    foreach (DataRow row in tabla.Rows)
                    {
                        Estructuras.TablaPadre1 itemtabla = new Estructuras.TablaPadre1();
                        itemtabla.id = System.Convert.ToInt32(row["id"].ToString());
                        itemtabla.nombre = row["nombre"].ToString();

                        response.Add(itemtabla);
                    }
                    connection.Close();
                }
                else
                {

                }
            }
            catch (SqlException exceptionSql)
            {
                MessageBox.Show(exceptionSql.Message);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

            return response;
        }

        public Estructuras.TablaPadre1 LoadData(string stringSQL,int idRegistro)
        {
            Estructuras.TablaPadre1 response = new Estructuras.TablaPadre1();

            SqlConnection connection = new SqlConnection(strCadenaConexion);
            try
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    //Aqui Ya prodre hacer select, insert y etc.

                    //string stringSQL = ;
                    SqlCommand sqlCommand = new SqlCommand(stringSQL, connection);
                    sqlCommand.Parameters.AddWithValue("@id", idRegistro);
                    SqlDataAdapter adapter = new SqlDataAdapter(sqlCommand);

                    DataTable tabla = new DataTable();

                    adapter.Fill(tabla);


                    foreach (DataRow row in tabla.Rows)
                    {
                        response.id = System.Convert.ToInt32(row["id"].ToString());
                        response.nombre = row["nombre"].ToString();
                    }

                    connection.Close();

                }
                else
                {

                }
            }
            catch (SqlException exceptionSql)
            {
                MessageBox.Show(exceptionSql.Message);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
            }

            return response;
        }

        public bool insertData(Estructuras.TablaPadre1 itemRegistro, string stringSQL)
        {
            bool retornevalor = false;
            SqlConnection connection = new SqlConnection(strCadenaConexion);
            try
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    //string stringSQL = $"INSERT INTO [dbo].[TablaPrueba1] ([id],[nombre]) VALUES (" + textBox1.Text + ",'" + textBox2.Text + "'); ";

                    SqlTransaction transaction;
                    transaction = connection.BeginTransaction();

                    //string stringSQL = $"INSERT INTO [dbo].[TablaPrueba1] ([id],[nombre]) VALUES (@id,@nombre); ";
                    SqlCommand sqlCommand = new SqlCommand(stringSQL, connection);
                    sqlCommand.Parameters.AddWithValue("@id", itemRegistro.id);
                    sqlCommand.Parameters.AddWithValue("@nombre", itemRegistro.nombre);
                    sqlCommand.Transaction = transaction;
                    int filasAfectadas = sqlCommand.ExecuteNonQuery();

                    try
                    {
                        transaction.Commit();
                        MessageBox.Show(filasAfectadas.ToString());
                        retornevalor = true;
                    }
                    catch (SqlException exceptionTransaction)
                    {
                        transaction.Rollback();
                        retornevalor = false;
                    }
                    connection.Close();
                }


            }
            catch (SqlException exceptionSql)
            {

                MessageBox.Show(exceptionSql.Message);
                retornevalor = false;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                retornevalor = false;
            }
            finally
            {

            }
            return retornevalor;
        }


        public bool updateData(Estructuras.TablaPadre1 registro , int idRegistro)
        {
            bool retornevalor = false;
            SqlConnection connection = new SqlConnection(strCadenaConexion);
            try
            {
                connection.Open();

                if (connection.State == ConnectionState.Open)
                {
                    SqlTransaction transaction;
                    transaction = connection.BeginTransaction();

                    string stringSQL = $"UPDATE [dbo].[TablaPrueba1] SET [id]=@id,[nombre]=@nombre WHERE [id]=@idActual;";
                    SqlCommand sqlCommand = new SqlCommand(stringSQL, connection);
                    sqlCommand.Parameters.AddWithValue("@id", registro.id);
                    sqlCommand.Parameters.AddWithValue("@nombre", registro.nombre);
                    sqlCommand.Parameters.AddWithValue("@idActual", idRegistro);
                    sqlCommand.Transaction = transaction;

                    int filasAfectadas = sqlCommand.ExecuteNonQuery();
                    try
                    {
                        transaction.Commit();
                        MessageBox.Show(filasAfectadas.ToString());
                        retornevalor = true;
                    }
                    catch (SqlException exceptionTransaction)
                    {
                        transaction.Rollback();
                        retornevalor = false;
                    }
                    connection.Close();
                }


            }
            catch (SqlException exceptionSql)
            {

                MessageBox.Show(exceptionSql.Message);
                retornevalor = false;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                retornevalor = false;
            }
            finally
            {

            }
            return retornevalor;
        }
    }
}
