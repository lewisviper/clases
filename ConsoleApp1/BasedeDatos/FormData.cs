﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BasedeDatos
{
    public partial class FormData : Form
    {
        public int idRegistro = 0;
       
        public FormData()
        {
            InitializeComponent();
        }

        private void FormData_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        void LoadData()
        {
            Estructuras.TablaPadre1 tablaPadre1 = new Estructuras.TablaPadre1();

            ManejoBD manejoBD = new ManejoBD();
            tablaPadre1 = manejoBD.LoadData("SELECT ID, NOMBRE FROM TABLAPRUEBA1 WHERE ID=@id;", this.idRegistro);

            textBox1.Text = tablaPadre1.id.ToString();
            textBox2.Text = tablaPadre1.nombre;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Estructuras.TablaPadre1 itemtabla = new Estructuras.TablaPadre1();
            itemtabla.id = System.Convert.ToInt32(textBox1.Text);
            itemtabla.nombre = textBox2.Text;

            ManejoBD manejoBD = new ManejoBD();
            manejoBD.updateData(itemtabla, this.idRegistro);
        }
    }
}
