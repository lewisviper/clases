﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

//Libreria Standard Sql
using System.Data;

//Libreria Standard de Sql Server
using System.Data.SqlClient;

using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BasedeDatos
{
    public partial class Form1 : Form
    {
        

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadData();
            
            
        }

        void LoadData()
        {
            List<Estructuras.TablaPadre1> tablaPadre1 = new List<Estructuras.TablaPadre1>();

            ManejoBD manejoBD = new ManejoBD();
            tablaPadre1 =  manejoBD.LoadData("SELECT ID, NOMBRE FROM TABLAPRUEBA1;");

            listView1.Items.Clear();
            foreach (Estructuras.TablaPadre1 row in tablaPadre1)
            {
                ListViewItem item = new ListViewItem();
                item.Name = row.id.ToString();
                item.Text = row.id.ToString();
                item.SubItems.Add(row.nombre);
                listView1.Items.Add(item);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ManejoBD manejoBD = new ManejoBD();

            Estructuras.TablaPadre1 itemtabla = new Estructuras.TablaPadre1();
            itemtabla.id = System.Convert.ToInt32(textBox1.Text);
            itemtabla.nombre = textBox2.Text;

            if (!manejoBD.insertData(null , $"INSERT INTO [dbo].[TablaPrueba1] ([id],[nombre]) VALUES (@id,@nombre); ")) MessageBox.Show("Ocurrio un error al insertar data");
                
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (listView1.SelectedItems.Count > 0)
            {
                FormData formulario = new FormData();
                formulario.idRegistro = Convert.ToInt32(listView1.SelectedItems[0].Name);
                formulario.Text = "Editar Registro: " + formulario.idRegistro;
                formulario.Show();
            }
        }
    }
}
