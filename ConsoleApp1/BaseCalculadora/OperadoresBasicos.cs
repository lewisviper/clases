﻿using System;

namespace BaseCalculadora
{
    public class OperadoresBasicos : Base
    {
        public double Seno(double numero)
        {
            return Math.Sin(numero);
        }
    }
}
