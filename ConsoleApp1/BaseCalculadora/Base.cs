﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BaseCalculadora
{
    public class Base
    {
        public double sumar(double valor1, double valor2)
        {
            double resultado = valor1 + valor2;
            return resultado;
        }

        public double sumar(string valor1, string valor2)
        {
            double resultado = System.Convert.ToDouble(valor1) + System.Convert.ToDouble(valor2);
            return resultado;
        }

        public double division(double valor1, double valor2)
        {
            double resultado = valor1 / valor2;
            return resultado;
        }

        public double resta(double valor1, double valor2)
        {
            double resultado = valor1 - valor2;
            return resultado;
        }

        public double multiplicacion(double valor1, double valor2)
        {
            double resultado = valor1 * valor2;
            return resultado;
        }
    }
}
