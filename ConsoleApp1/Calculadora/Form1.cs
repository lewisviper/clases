﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BaseCalculadora;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        enum listaOperadores
        {
            ninguno = 0,
            multiplicacion = 1,
            division = 2,
            suma = 3,
            resta = 4
        }

        bool estaNumero1 = true;
        string numero1 = "";
        string numero2 = "";
        listaOperadores operador = listaOperadores.ninguno ;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        void imprimirLabel()
        {
            if (estaNumero1) LabelNumero.Text = numero1;
            else LabelNumero.Text = numero2;
        }

        bool valideNumero(string numero)
        {
            double retNum;
            bool isNum = Double.TryParse(Convert.ToString(numero), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);

            if (isNum)
            {
                if (numero.Length > 1 && System.Convert.ToDouble(numero) == 0)
                {
                    isNum = false;
                }
            }
            return isNum;

        }

        void addNumero(string numero)
        {
            if (estaNumero1)
            {
                if (valideNumero(numero1 + numero))
                {
                    
                    numero1 = numero1 + numero;
                    if (numero1.Length > 1 && numero1.Substring(0, 1) == "0") numero1 = numero1.Substring(1, numero1.Length - 1);
                }
            }
            else
            {
                if (valideNumero(numero2 + numero))
                {
                    numero2 = numero2 + numero;
                    if (numero2.Length > 1 && numero2.Substring(0, 1) == "0") numero2 = numero2.Substring(1, numero2.Length - 1);
                }
            }

            imprimirLabel();
        }

        private void botonCero_Click(object sender, EventArgs e)
        {
            addNumero("0");
        }

        private void botonUno_Click(object sender, EventArgs e)
        {
            addNumero("1");
        }

        private void button9_Click(object sender, EventArgs e)
        {
            addNumero("2");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            addNumero("3");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            addNumero("4");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            addNumero("5");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            addNumero("6");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            addNumero("7");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            addNumero("8");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            addNumero("9");
        }

        private void button16_Click(object sender, EventArgs e)
        {
            operador = listaOperadores.division;
            estaNumero1 = false;
        }

        private void button15_Click(object sender, EventArgs e)
        {
            operador = listaOperadores.multiplicacion;
            estaNumero1 = false;
        }

        private void button14_Click(object sender, EventArgs e)
        {
            operador = listaOperadores.resta;
            estaNumero1 = false;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            operador = listaOperadores.suma;
            estaNumero1 = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            OperadoresBasicos operadores = new OperadoresBasicos();
            switch (operador)
            {
                case listaOperadores.division:
                    LabelNumero.Text = operadores.division(System.Convert.ToDouble( numero1), System.Convert.ToDouble(numero2)).ToString();
                    break;
                case listaOperadores.multiplicacion:
                    LabelNumero.Text = operadores.multiplicacion(System.Convert.ToDouble(numero1), System.Convert.ToDouble(numero2)).ToString();
                    break;
                case listaOperadores.resta:
                    LabelNumero.Text = operadores.resta(System.Convert.ToDouble(numero1), System.Convert.ToDouble(numero2)).ToString();
                    break;
                case listaOperadores.suma:
                    //LabelNumero.Text = operadores.sumar(System.Convert.ToDouble(numero1), System.Convert.ToDouble(numero2)).ToString();

                    LabelNumero.Text = operadores.sumar(numero1, numero2).ToString();

                    LabelNumero.Text = operadores.Seno(System.Convert.ToDouble(numero1)).ToString();
                    break;
                default:
                    imprimirLabel();
                    break;
            }
        }
    }
}
