import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FormsComponent } from './components/forms/forms.component';
import { MainHeaderComponent } from './template/main-header/main-header.component';


const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'dashboard',
  },
  {
    path: '',
    children: [           
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          title: 'DashBoard',
        }
      },
      {
        path: 'forms',
        component: FormsComponent,
        data: {
          title: 'Formularios',
        }
      },
      {
        path: 'sidebar',
        component: MainHeaderComponent,
        data:
        {
          title: 'Encabezado'
        }
      }

    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
