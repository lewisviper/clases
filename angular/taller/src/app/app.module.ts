import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FormsComponent } from './components/forms/forms.component';
import { MainHeaderComponent } from './template/main-header/main-header.component';
import { MainSidebarComponent } from './template/main-sidebar/main-sidebar.component';
import { MainFooterComponent } from './template/main-footer/main-footer.component';
import { MainControlSidebarComponent } from './template/main-control-sidebar/main-control-sidebar.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    FormsComponent,
    MainHeaderComponent,
    MainSidebarComponent,
    MainFooterComponent,
    MainControlSidebarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
